import { Routes } from '@angular/router';
import { CatListComponent } from './pets/cat-list/cat-list.component';

export const APP_ROUTES: Routes = [
  { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: 'home', component: CatListComponent },
];
