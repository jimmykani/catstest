import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
@Injectable()
export class DataService {
  private REST_API_SERVER =
    'http://agl-developer-test.azurewebsites.net/people.json';
  constructor(private httpClient: HttpClient) {}
  getPets(): Observable<HttpResponse<any>> {
    return this.httpClient.get(this.REST_API_SERVER, { observe: 'response' });
  }
}
