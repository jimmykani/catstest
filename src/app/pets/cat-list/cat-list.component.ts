import { Component, OnDestroy, OnInit } from '@angular/core';
import { Subscription } from 'rxjs';
import { DataService } from 'src/app/services/dataService';

@Component({
  selector: 'app-cat-list',
  templateUrl: './cat-list.component.html',
  styleUrls: ['./cat-list.component.scss'],
})
export class CatListComponent implements OnInit, OnDestroy {
  malePets = [];
  femalePets = [];
  $pets: Subscription;
  constructor(private dataService: DataService) {}
  ngOnDestroy(): void {
    this.$pets.unsubscribe();
  }

  ngOnInit() {
    this.$pets = this.dataService.getPets().subscribe((data) => {
      data.body
        .filter((d: { gender: string }) => d.gender === 'Male')
        .map((p) => {
          if (p.pets) {
            this.malePets = [
              ...this.malePets,
              ...p.pets.filter((p) => p.type === 'Cat'),
            ];
          }
        });
      data.body
        .filter((d) => d.gender === 'Female')
        .map((p) => {
          if (p.pets) {
            this.femalePets = [
              ...this.femalePets,
              ...p.pets.filter((p) => p.type === 'Cat'),
            ];
          }
        });
      this.malePets.sort((a, b) => {
        return a.name.localeCompare(b.name);
      });
      this.femalePets.sort((a, b) => {
        return a.name.localeCompare(b.name);
      });
    });
  }
}
