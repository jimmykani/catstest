/* tslint:disable:no-unused-variable */
import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { DebugElement } from '@angular/core';

import { CatListComponent } from './cat-list.component';
import { DataService } from 'src/app/services/dataService';
import { HttpClientModule } from '@angular/common/http';
import { Observable } from 'rxjs';

describe('CatListComponent', () => {
  let component: CatListComponent;
  let fixture: ComponentFixture<CatListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientModule],
      declarations: [CatListComponent],
      providers: [DataService],
    }).compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CatListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  it('ngOnInit', () => {
    const dataServiceSpy = jasmine.createSpyObj('DataService', ['getPets']);
    const body = new Observable<any>((observer) => {
      observer.next({
        body: [
          {
            name: 'Bob',
            gender: 'Male',
            pets: [
              {
                name: 'Garfield',
                type: 'Cat',
              },
              {
                name: 'Fido',
                type: 'Dog',
              },
            ],
          },
          {
            name: 'Jennifer',
            gender: 'Female',
            pets: [
              {
                name: 'Garfield',
                type: 'Cat',
              },
            ],
          },
        ],
      });
      observer.complete();
    });
    dataServiceSpy.getPets.and.returnValue(body);
    // subscribe to the observable
    const comp = new CatListComponent(dataServiceSpy);
    comp.ngOnInit();
    expect(component).toBeTruthy();
  });
});
